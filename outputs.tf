# Cluster Info and Creds
output "kubernetes_endpoint" {
  sensitive = true
  value = module.kubernetes-engine.endpoint
}

output "client_token" {
  sensitive = true
  value = base64encode(data.google_client_config.default.access_token)
}

output "ca_certificate" {
  value = module.kubernetes-engine.ca_certificate
}

output "service_account" {
  value = module.kubernetes-engine.service_account
}
