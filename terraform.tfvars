project_id            = "radar-work-trial"
cluster_name          = "radar-work-trial-gke"
region                = "us-central1"
zones                 = ["us-central1-a"]
default_node_count    = 2
ethereum_node_count   = 1
default_machine_type  = "n1-standard-2"
ethereum_machine_type = "n1-standard-4"
