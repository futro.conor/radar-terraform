# Add repo info
data "gitlab_project" "radar-helm" {
  id = 19628122
}

data "gitlab_project" "radar-monitoring" {
  id = 19635062
}

# Attach cluster to repos
resource gitlab_project_cluster "radar-work-trial-gke" {
  project                       = data.gitlab_project.radar-helm.id
  name                          = var.cluster_name
  managed                       = false
  kubernetes_api_url            = "https://${module.kubernetes-engine.endpoint}"
  kubernetes_token              = data.google_client_config.default.access_token
  kubernetes_ca_cert            = base64decode(module.kubernetes-engine.ca_certificate)
  kubernetes_authorization_type = "rbac"
}

resource gitlab_project_cluster "radar-work-trial-gke-2" {
  project                       = data.gitlab_project.radar-monitoring.id
  name                          = var.cluster_name
  managed                       = false
  kubernetes_api_url            = "https://${module.kubernetes-engine.endpoint}"
  kubernetes_token              = data.google_client_config.default.access_token
  kubernetes_ca_cert            = base64decode(module.kubernetes-engine.ca_certificate)
  kubernetes_authorization_type = "rbac"
}
