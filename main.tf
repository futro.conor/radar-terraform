# Utilize terraform-google-kubernetes ( https://github.com/terraform-google-modules/terraform-google-kubernetes-engine ) for GKE cluster

module "kubernetes-engine" {
  source                   = "terraform-google-modules/kubernetes-engine/google//modules/beta-public-cluster"
  version                  = "9.4.0"
  project_id               = var.project_id
  name                     = var.cluster_name
  regional                 = false
  region                   = var.region
  zones                    = var.zones
  network                  = module.gcp-network.network_name
  subnetwork               = module.gcp-network.subnets_names[0]
  ip_range_pods            = var.ip_range_pods_name
  ip_range_services        = var.ip_range_services_name
  remove_default_node_pool = true
  grant_registry_access    = true

# Create separate node pool for OpenEthereum
  node_pools = [
    {
      name         = "default-pool"
      machine_type = var.default_machine_type
      node_count   = var.default_node_count
      min_count    = var.default_node_count
      max_count    = var.default_node_count
      preemptible  = false
    },
    {
      name         = "openeth-pool"
      machine_type = var.ethereum_machine_type
      node_count   = var.ethereum_node_count
      min_count    = var.ethereum_node_count
      max_count    = var.ethereum_node_count
      preemptible  = false
    },
  ]

  node_pools_labels = {
    all = {
      project = "RADAR"
    }
    openeth-pool = {
      ethereum = true
    }
  }

# Taint OpenEthereum node pool to ensure single tenancy
  node_pools_taints = {
    openeth-pool = [
      {
        key    = "blockchain"
        value  = "ethereum"
        effect = "NO_SCHEDULE"
      },
    ]
  }
}

data "google_client_config" "default" {
}
