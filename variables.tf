# Project Variables
variable "project_id" {
  type        = string
  description = "Project ID to host cluster in"
}

variable "cluster_name" {
  type        = string
  description = "Cluster name"
}

variable "region" {
  type        = string
  description = "Region to host cluster in"
}

variable "zones" {
  type        = list(string)
  description = "Zone(s) to host cluster in"
}

variable "ip_range_pods_name" {
  type        = string
  description = "Secondary IP range for pods"
  default     = "ip-range-pods"
}

variable "ip_range_services_name" {
  type        = string
  description = "Secondary IP range for services"
  default     = "ip-range-svc"
}

variable "default_node_count" {
  type        = string
  description = "Initial, min, and max node count for default pool"
}

variable "ethereum_node_count" {
  type        = string
  description = "Initial, min, and max node count for openeth pool"
}

variable "default_machine_type" {
  type        = string
  description = "Instance type for default pool"
}

variable "ethereum_machine_type" {
  type        = string
  description = "Instance type for openeth pool"
}

variable "gitlab_token" {
  type        = string
  description = "GitLab API Token"
}
