# RADAR Terraform
## Description and Rationale
This repository relies on external modules for most of the heavy lifting. Utilizing these modules for creating the network and cluster has several benefits. Namely leveraging a pre-existing, well-maintained code base allows for benefiting from the work that has already gone into testing and as a result significantly reduces the amount of time needed to bring up robust infrastructure. Additionally the contents of `gitlab.tf` will connect the cluster to the other repositories that will be making use of it through GitLab CI to ease credential management and configuration.

## CI/CD
The pipeline in this repository will automatically run through a `terrform plan` when a commit is made. When a merge request is created it will attach the output of the plan as a comment, allowing for `terraform apply` to be manually initiated through the pipeline after it runs again when the merge is approved.

