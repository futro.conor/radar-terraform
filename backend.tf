# Backend and provider config
terraform {
  backend "http" {
  }
}

provider "google-beta" {
  project = var.project_id
  region  = var.region
}

provider "gitlab" {
  token = var.gitlab_token
}
